package com.castellanos.bottombaryefer;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {

    FrameLayout contenedorFragmentos;

    BottomNavigationView menuInferior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menuInferior= findViewById(R.id.menuInferior);

        //crear un listner para capturar los eventos al seleccionar un item del menu

        menuInferior.setOnItemSelectedListener(
                //requiere como argumento un objeto de la clase navigationBarView

                new NavigationBarView.OnItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        //aca se programa lo que sucede lo que hace click en algun item o icono
                        // se toma el id y tomar la desición
                        //1. obtener el id del item seleccionado

                        int id=item.getItemId();
                        Log.d(TAG, String.valueOf(id));

                        //hacer un switc para los diferntes elementos

                        switch (id) {
                            case R.id.iCal:
                                //cargar el fragmento de la calculadora
                                break;

                            case R.id.iTeams:
                                //cargar el fragmento de la equipos de futbol
                                 break;

                            case R.id.iMap:
                                //cargar el fragmento de los mapas
                                break;
                        }
                        return false;
                    }
                }

        );



    }
}